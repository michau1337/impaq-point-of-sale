import io.scanner.ScannerListener;
import io.scanner.barcode.BarcodeScanner;
import io.display.Display;
import io.printer.Printer;
import product.exception.ProductNotFoundException;
import product.model.Product;
import product.service.ProductService;
import sale.Receipt;
import sale.ReceiptPrinter;
import sale.Sale;

import java.io.IOException;
import java.util.Scanner;

public class Pos implements ScannerListener {

    private BarcodeScanner barcodeScanner;

    private Display display;

    private ProductService productService;

    private Sale sale;

    private Printer printer;

    private boolean working = true;

    public Pos(BarcodeScanner barcodeScanner, Display display, Printer printer, ProductService productService) {
        this.barcodeScanner = barcodeScanner;
        this.productService = productService;
        this.printer = printer;
        this.display = display;
        this.sale = new Sale();

        barcodeScanner.setScanListener(this);
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (working && scanner.hasNextLine()) {
            barcodeScanner.scanStub(scanner.nextLine());
        }
    }

    @Override
    public void onBarcodeScanned(String barcode) {
        switch (barcode) {
            case PosConstants.EXIT_BARCODE:
                onExit();
                break;

            case PosConstants.INVALID_BARCODE:
                display.printText(PosConstants.INVALID_BARCODE_MESSAGE);
                break;

            default:
                try {
                    tryAddProduct(barcode);
                } catch (ProductNotFoundException e) {
                    display.printText(e.getLocalizedMessage());
                    // log(e);
                }
        }
    }

    private void tryAddProduct(String barcode) {
        Product product = productService.getProductByBarcode(barcode)
                .orElseThrow(
                        () -> new ProductNotFoundException(PosConstants.PRODUCT_NOT_FOUND_MESSAGE)
                );

        sale.add(product);

        display.printText(String.format("%s %.2f", product.getName(), product.getPrice()));
    }

    private void onExit() {
        Receipt receipt = sale.generateReceipt();

        printReceipt(printer, receipt);

        display.printText(String.format("Total: %.2f", receipt.getTotalPrice()));

        sale.reset();
        working = false;
    }

    private void printReceipt(Printer printer, Receipt receipt) {
        try (ReceiptPrinter receiptPrinter = new ReceiptPrinter(printer)) {
            receiptPrinter.print(receipt);
        } catch (IOException e) {
            display.printText(e.getLocalizedMessage());
            // log(e);
        }
    }
}
