package product.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {

    private final String name;

    private final String barcode;

    private final BigDecimal price;

    public Product(String name, String barcode, BigDecimal price) {
        this.name = name;
        this.barcode = barcode;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getBarcode() {
        return barcode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object that) {
        return (this == that || that instanceof Product)
                && Objects.equals(barcode, ((Product) that).barcode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(barcode);
    }
}
