package product.service;

import product.model.Product;
import product.repository.ProductRepository;

import java.util.Optional;

public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Optional<Product> getProductByBarcode(String barcode) {
        return productRepository.findProductByBarcode(barcode);
    }
}
