package product.service;

import product.model.Product;

import java.util.Optional;

public interface ProductService {

    Optional<Product> getProductByBarcode(String barcode);
}
