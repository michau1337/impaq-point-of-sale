package product.repository;

import product.model.Product;

import java.util.Optional;

public interface ProductRepository {

    Optional<Product> findProductByBarcode(String barcode);
}
