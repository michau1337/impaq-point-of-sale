package product.repository;

import product.model.Product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;

public class ProductRepositoryImpl implements ProductRepository {

    private HashMap<String, Product> products;

    public ProductRepositoryImpl() {
        products = new HashMap<>();

        products.put("111222", new Product("Banana", "1112222", new BigDecimal(2.99)));
        products.put("222333", new Product("Apple", "222333", new BigDecimal(1.51)));
        products.put("333444", new Product("Watermelon", "333444", new BigDecimal(12.01)));
        products.put("444555", new Product("Grapefruit", "444555", new BigDecimal(7.77)));
    }

    @Override
    public Optional<Product> findProductByBarcode(String barcode) {
        Product product = products.get(barcode);
        return Optional.ofNullable(product);
    }
}
