import io.display.BasicLcdDisplay;
import io.printer.BasicLinePrinter;
import io.scanner.barcode.BasicBarcodeScanner;
import product.repository.ProductRepositoryImpl;
import product.service.ProductServiceImpl;

public class Main {
    public static void main(String[] args) {

        Pos pos = new Pos(
                new BasicBarcodeScanner(),
                new BasicLcdDisplay(),
                new BasicLinePrinter(),
                new ProductServiceImpl(new ProductRepositoryImpl())
        );

        pos.start();
    }
}
