public class PosConstants {
    public static final String INVALID_BARCODE_MESSAGE = "Invalid bar-code";
    public static final String PRODUCT_NOT_FOUND_MESSAGE = "Product not found";

    public static final String INVALID_BARCODE = "";
    public static final String EXIT_BARCODE = "exit";
}
