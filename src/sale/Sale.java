package sale;

import product.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Sale {

    private Map<Product, Integer> products;

    public Sale() {
        products = new HashMap<>();
    }

    public void reset() {
        products.clear();
    }

    public void add(Product product) {
        if (saleHas(product)) {
            increaseProductQuantity(product);
        } else {
            products.put(product, 1);
        }
    }

    public Receipt generateReceipt() {
        List<ReceiptProduct> receiptProducts = generateReceiptProducts();
        return new Receipt(receiptProducts);
    }

    private void increaseProductQuantity(Product product) {
        products.computeIfPresent(product, (p, quantity) -> quantity + 1);
    }

    private List<ReceiptProduct> generateReceiptProducts() {
        return products.entrySet()
                .stream()
                .map(item -> {
                    Product product = item.getKey();
                    int quantity = item.getValue();

                    return new ReceiptProduct.Builder()
                            .setName(product.getName())
                            .setPrice(product.getPrice())
                            .setQuantity(quantity)
                            .build();
                })
                .collect(Collectors.toList());
    }

    private boolean saleHas(Product product) {
        return products.containsKey(product);
    }
}
