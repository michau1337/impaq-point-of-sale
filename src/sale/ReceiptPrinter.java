package sale;

import io.printer.Printer;

import java.io.Closeable;
import java.io.IOException;

public class ReceiptPrinter implements Closeable {

    private Printer printer;

    public ReceiptPrinter(Printer printer) {
        this.printer = printer;
    }

    public void print(Receipt receipt) {

        printer.printLine("\n--------- Receipt ---------");

        int i = 1;
        for (ReceiptProduct product : receipt.getProducts()) {
            printer.printLine(String.format("%d. %-10s  %.2f x %d = %.2f",
                    i, product.getName(), product.getPrice(), product.getQuantity(), product.getTotalPrice()));
            i++;
        }

        printer.printLine(String.format("\nTotal price %.2f", receipt.getTotalPrice()));
        printer.printLine("--------- End of receipt ---------\n");
    }

    @Override
    public void close() throws IOException {
        printer.close();
    }
}
