package sale;

import java.math.BigDecimal;

public class ReceiptProduct {

    private final String name;

    private final BigDecimal price;

    private final int quantity;

    public ReceiptProduct(String name, BigDecimal price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return price.multiply(new BigDecimal(quantity));
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public static class Builder {
        private String name;

        private BigDecimal price;

        private int quantity;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public ReceiptProduct build() {
            return new ReceiptProduct(name, price, quantity);
        }
    }
}
