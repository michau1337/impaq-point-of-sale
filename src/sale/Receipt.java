package sale;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class Receipt {

    private final List<ReceiptProduct> products;

    private final BigDecimal totalPrice;

    public Receipt(List<ReceiptProduct> products) {
        this.products = products;
        this.totalPrice = calculateTotalPrice(products);
    }

    public List<ReceiptProduct> getProducts() {
        return Collections.unmodifiableList(products);
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    private BigDecimal calculateTotalPrice(List<ReceiptProduct> products) {
        return products.stream()
                .map(ReceiptProduct::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
