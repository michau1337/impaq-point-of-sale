package io.display;

public class BasicLcdDisplay implements Display {

    @Override
    public void printText(String text) {
        System.out.println(String.format("LCD Display: [%s]", text));
    }
}
