package io.printer;

import java.io.IOException;

public class BasicLinePrinter implements Printer {

    @Override
    public void printLine(String line) {
        System.out.println(line);
    }

    @Override
    public void close() throws IOException {
        System.out.flush();
        System.out.close();
    }
}
