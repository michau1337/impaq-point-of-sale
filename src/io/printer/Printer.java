package io.printer;

import java.io.Closeable;

public interface Printer extends Closeable {

    void printLine(String line);
}
