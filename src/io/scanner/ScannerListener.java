package io.scanner;

public interface ScannerListener {

    void onBarcodeScanned(String barcode);
}
