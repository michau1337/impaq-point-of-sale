package io.scanner.barcode;

import io.scanner.ScannerListener;

public interface BarcodeScanner {

    void setScanListener(ScannerListener listener);

    void scanStub(String scan);
}
