package io.scanner.barcode;

import io.scanner.ScannerListener;

public class BasicBarcodeScanner implements BarcodeScanner {

    private ScannerListener scanListener;

    @Override
    public void setScanListener(ScannerListener listener) {
        scanListener = listener;
    }

    public void scanStub(String scan) {
        onScan(scan.trim());
    }

    private void onScan(String scan) {
        if (scanListener != null) {
            scanListener.onBarcodeScanned(scan);
        }
    }
}
